package de.tu_darmstadt.informatik.tk.olir;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;

public class ReadingActivity extends AppCompatActivity {

    String stringTrue = "";
    String stringFalse = "";
    String name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reading);



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("2. Place the Object");


    }

    @Override
    public void onResume() {
        super.onResume();

        String[] file = getIntent().getStringArrayExtra("file");
        Parser.parseFile(file, this);
        findViewById(R.id.drawView).invalidate();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Intent intent = new Intent(this, ObjectsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(this, ObjectsActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }

    public void onDetected(View view) {
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("information", stringTrue);
        intent.putExtra("result", true);
        intent.putExtra("name", name);
        startActivity(intent);
    }

    public void onNotDetected(View view) {
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("information", stringFalse);
        intent.putExtra("result", false);
        intent.putExtra("name", name);
        startActivity(intent);
    }
}
