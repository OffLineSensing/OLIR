package de.tu_darmstadt.informatik.tk.olir;

import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class HistoryActivity extends AppCompatActivity {

    private ArrayList<String> lines;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Previous Sensors");

        parseHistory();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.clear_history) {
            try {
                OutputStreamWriter writer = new OutputStreamWriter(openFileOutput("history.txt", MODE_PRIVATE));
                String content = "";
                writer.write(content);
                writer.flush();
                writer.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            TableLayout table = findViewById(R.id.tableLayout);
            table.removeAllViews();
        }
        else if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(this, ObjectsActivity.class);
            startActivity(intent);
            return true;
        }

        return true;
    }

    private void parseHistory() {
        lines = new ArrayList<>();
        ContextWrapper cw = new ContextWrapper(this);
        File file = new File(cw.getFilesDir(), "history.txt");

        TableLayout table = findViewById(R.id.tableLayout);

        try {
            if (file.exists()) {
                FileInputStream is = new FileInputStream(file);

                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String line = reader.readLine();
                while (line != null) {
                    lines.add(line);

                    line = reader.readLine();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = lines.size()-1; i >= 0; i--) {
            String line = lines.get(i);

            String time = line.substring(0, line.indexOf("|"));
            line = line.substring(line.indexOf("|") + 1);
            String name = line.substring(0, line.indexOf("|"));
            String result = line.substring(line.indexOf("|") + 1);

            TableRow row = new TableRow(this);
            row.setPadding(20,30,20,30);
            row.setTag(i);
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openHistoryEntry(v);
                }
            });
            TextView textView = new TextView(this);
            textView.setText(name);
            textView.setTextSize(20);

            Drawable drawable;

            if (name.toLowerCase().contains("load")) {
                drawable = getResources().getDrawable(R.mipmap.load);
            } else if (name.toLowerCase().contains("temperature")) {
                if (name.toLowerCase().contains("rising")) {
                    drawable = getResources().getDrawable(R.mipmap.temp_rising);
                } else {
                    drawable = getResources().getDrawable(R.mipmap.temp_falling);
                }
            } else if (name.toLowerCase().contains("acceleration")) {
                drawable = getResources().getDrawable(R.mipmap.acceleration);
            } else if (name.toLowerCase().contains("tilt")) {
                drawable = getResources().getDrawable(R.mipmap.rotate90);
            } else if (name.toLowerCase().contains("flip")) {
                drawable = getResources().getDrawable(R.mipmap.flip);
            } else if (name.toLowerCase().contains("pressure")) {
                drawable = getResources().getDrawable(R.mipmap.pressure);
            } else {
                drawable = getResources().getDrawable(R.mipmap.cog);
            }


            float factor = getResources().getDisplayMetrics().density;
            drawable.setBounds(0, 0, (int) (20 * factor), (int) (20 * factor));
            textView.setCompoundDrawables(drawable, null, null, null);
            textView.setCompoundDrawablePadding(10);
            textView.setPadding(10, 10, 10, 10);
            row.addView(textView);


            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
                Date date = sdf.parse(time);
                sdf.applyPattern("yyyy/MM/dd hh:mm:ss");
                time = sdf.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (Boolean.parseBoolean(result))
                result = "state changed";
            else
                result = "state unchanged";

            TextView dateTV = new TextView(this);
            dateTV.setText("Result: " + result + "\n" + time);
            dateTV.setTextSize(16);
            dateTV.setMaxLines(2);
            dateTV.setGravity(Gravity.RIGHT);
            TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
            params.weight = 1;
            params.gravity = Gravity.BOTTOM;
            dateTV.setLayoutParams(params);
            row.addView(dateTV);


            table.addView(row);


            View v = new View(this);
            v.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    2
            ));
            v.setBackgroundColor(Color.parseColor("#000000"));
            table.addView(v);
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Intent intent = new Intent(this, ObjectsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    public void openHistoryEntry(View view) {
        TableRow row = (TableRow) view;
        int index = (Integer) row.getTag();
        String line = lines.get(index);
        String date = line.substring(0, line.indexOf("|"));
        line = line.substring(line.indexOf("|") + 1);
        String name = line.substring(0, line.indexOf("|"));
        String result = line.substring(line.indexOf("|") + 1);

        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("time", date);
        intent.putExtra("name", name);
        intent.putExtra("result", Boolean.parseBoolean(result));
        startActivity(intent);
    }
}
