package de.tu_darmstadt.informatik.tk.olir;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ObjectsActivity extends AppCompatActivity {

    static final int FILE_SELECT_CODE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_objects);

        getSupportActionBar().setTitle("1. Choose a Sensor");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        if (menuItem.getItemId() == R.id.show_history) {
                            Intent intent = new Intent(ObjectsActivity.this, HistoryActivity.class);
                            startActivity(intent);
                        }

                        return true;
                    }
                });
    }

    public void openFile(View view) {
        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
        chooseFile.setType("*/*");
        chooseFile = Intent.createChooser(chooseFile, "Choose a file");
        startActivityForResult(chooseFile, FILE_SELECT_CODE);
    }

    //FROM http://stackoverflow.com/questions/7856959/android-file-chooser 14.3.17 13:00
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    // Get the path
                    File file = new File(uri.getPath());
                    String fileName = uri.toString();
                    String extension = "";

                    int i = fileName.lastIndexOf('.');
                    if (i > 0) {
                        extension = fileName.substring(i+1);
                    }
                    if (extension.equals("txt")) {
                        try {
                            openReadingActivityWith(new FileInputStream(file));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void openLoad(View view) {
        openReadingActivityWith(getResources().openRawResource(R.raw.object_specs_load));
    }

    public void openPressure(View view) {
        openReadingActivityWith(getResources().openRawResource(R.raw.object_specs_pressure));
    }

    public void openAcceleration(View view) {
        openReadingActivityWith(getResources().openRawResource(R.raw.object_specs_acceleration));
    }

    public void openTilt90(View view) {
        openReadingActivityWith(getResources().openRawResource(R.raw.object_specs_tilt90));
    }

    public void openTilt180(View view) {
        openReadingActivityWith(getResources().openRawResource(R.raw.object_specs_tilt180));

    }

    public void openTempRising(View view) {
        openReadingActivityWith(getResources().openRawResource(R.raw.object_specs_temp_rising));
    }

    public void openTempFalling(View view) {
       openReadingActivityWith(getResources().openRawResource(R.raw.object_specs_temp_falling));
    }

    public void openReadingActivityWith(InputStream stream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        String[] content = new String[5];
        String line;
        int i = 0;
        try {
            while ((line = reader.readLine()) != null)  {
                content[i++] = line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(this, ReadingActivity.class);
        intent.putExtra("file", content);
        startActivity(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            finishAffinity();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.openDrawer(GravityCompat.START);
            return true;
        }
        return false;
    }
}
