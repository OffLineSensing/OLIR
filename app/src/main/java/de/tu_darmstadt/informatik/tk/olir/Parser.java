package de.tu_darmstadt.informatik.tk.olir;

import android.widget.TextView;


/**
 * Parses an objectSpecs.txt file
 * Created by Martin Herbers on 17.03.2017.
 */

public class Parser {


    public static void parseFile(String[] content, ReadingActivity activity) {
        //First line: Object title
        TextView textView = activity.findViewById(R.id.textView);
        String title = content[0].substring(content[0].indexOf(':')+1);
        textView.setText(title);
        activity.name = title;
        //Set the two status strings
        activity.stringTrue = content[3].substring(content[3].indexOf(':')+1);
        activity.stringFalse = content[4].substring(content[4].indexOf(':')+1);

        DrawView background = activity.findViewById(R.id.drawView);
        //Outer shapes
        String shapes = content[1].substring(content[1].indexOf(':')+1);
        background.clearShapes();
        int idx = shapes.indexOf(']')+1;
        while (idx > 0) {
            //Add every basic shape to the list that gets drawn
            background.addShape(shapes.substring(0, idx));
            shapes = shapes.substring(idx);
            idx = shapes.indexOf(']')+1;
        }

        background.invalidate();

        DrawView drawView = activity.findViewById(R.id.drawView);

        //Button size and position
        String buttonText = content[2].substring(content[2].indexOf(':')+1);
        if (buttonText.charAt(1) == 'C') {

            buttonText = buttonText.substring(3);
            int index = buttonText.indexOf(';');
            float x = Float.parseFloat(buttonText.substring(0, index));

            buttonText = buttonText.substring(index + 1);
            index = buttonText.indexOf(';');
            float y = Float.parseFloat(buttonText.substring(0, index));

            buttonText = buttonText.substring(index + 1);
            index = buttonText.length()-1;
            float diameter = Float.parseFloat(buttonText.substring(0, index));

            drawView.setPosition(x, y, diameter, diameter);

        } else if (buttonText.charAt(1) == 'R') {

            buttonText = buttonText.substring(3);
            int index = buttonText.indexOf(';');
            float x = Float.parseFloat(buttonText.substring(0, index));

            buttonText = buttonText.substring(index + 1);
            index = buttonText.indexOf(';');
            float y = Float.parseFloat(buttonText.substring(0, index));

            buttonText = buttonText.substring(index + 1);
            index = buttonText.indexOf(';');
            float width = Float.parseFloat(buttonText.substring(0, index));

            buttonText = buttonText.substring(index + 1);
            index = buttonText.length()-1;
            float height = Float.parseFloat(buttonText.substring(0, index));

           drawView.setPosition(x, y, width, height);
        }
    }
}
