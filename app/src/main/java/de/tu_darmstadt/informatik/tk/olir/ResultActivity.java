package de.tu_darmstadt.informatik.tk.olir;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String time = getIntent().getStringExtra("time");

        if (time == null) {
            getSupportActionBar().setTitle("3. Result");

            TextView textView = findViewById(R.id.resultTextView);
            textView.setText(getIntent().getStringExtra("information"));

            ImageView imageView = findViewById(R.id.resultImageView);

            final boolean result = getIntent().getExtras().getBoolean("result");
            if (result) {
                imageView.setImageResource(R.mipmap.checkmark);
            } else {
                imageView.setImageResource(R.mipmap.cross);
            }
            new Handler().post(new Runnable() {

                @Override
                public void run() {
                    saveToHistory(result);
                }
            });
        }
        else {
            getSupportActionBar().setTitle(getIntent().getStringExtra("name"));

            ImageView imageView = findViewById(R.id.resultImageView);

            final boolean result = getIntent().getExtras().getBoolean("result");
            if (result) {
                imageView.setImageResource(R.mipmap.checkmark);
            } else {
                imageView.setImageResource(R.mipmap.cross);
            }
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
                Date date = sdf.parse(time);
                sdf.applyPattern("yyyy/MM/dd hh:mm:ss");

                TextView textView = findViewById(R.id.resultTextView);
                textView.setText("From: " + sdf.format(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveToHistory(boolean result) {

        Date date = new Date();
        String dateFormat = new SimpleDateFormat("yyyyMMddhhmmss").format(date);
        try {
            OutputStreamWriter writer = new OutputStreamWriter(openFileOutput("history.txt", MODE_APPEND));
            String content = dateFormat + "|" + getIntent().getStringExtra("name") + "|" + result + "\n";
            writer.write(content);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Intent intent = new Intent(this, ObjectsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(this, ObjectsActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }
}
