package de.tu_darmstadt.informatik.tk.olir;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

/**
 * Area where the object's shape gets drawn
 * Created by Martin Herbers on 30.03.2017.
 */


public class DrawView extends View {

    /**
     * List of Strings where every String describes a basic shape, e.g. rectangle: [R;0;0;20;20]
     */
    ArrayList<String> shapes;
    /**
     * millimeter * scale = pixel
     */
    private float scale;

    private float electrodeX;
    private float electrodeY;
    private float electrodeWidth;
    private float electrodeHeight;

    private boolean statusDetected = false;
    private Handler handler;

    public DrawView(Context context) {
        super(context);
        clearShapes();
    }

    public DrawView(Context context, AttributeSet attribs) {
        super(context, attribs);
        clearShapes();
    }

    public DrawView(Context context, AttributeSet attribs, int defStyle) {
        super(context, attribs, defStyle);
        clearShapes();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            float touchX = event.getX();
            float touchY = event.getY();

            float baseX = getWidth()/2.0f;
            float baseY = getHeight()/2.0f;

            //Looking for width and height instead of width/2 nad height/2 in both direction is intended.
            //This functions as a larger area to catch the input if the user doesn't place the object perfectly
            if (touchX > baseX + (electrodeX - electrodeWidth) * scale &&
                    touchX < baseX + (electrodeX + electrodeWidth) * scale &&
                    touchY > baseY + (electrodeY - electrodeHeight) * scale &&
                    touchY < baseY + (electrodeY + electrodeHeight) * scale) {

                statusDetected = true;
            }
        }

        if (handler == null) {
            handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkStatus();
                }
            }, 300);
        }

        return super.onTouchEvent(event);
    }

    private void checkStatus() {
        ReadingActivity activity = (ReadingActivity) getContext();
        if (statusDetected)
            activity.onDetected(null);
        else
            activity.onNotDetected(null);

    }


    @Override
    public void onDraw(Canvas canvas) {
        scale = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, 1,
                getResources().getDisplayMetrics());

        //Middle point of the draw area
        float baseX = canvas.getWidth()/2.0f;
        float baseY = canvas.getHeight()/2.0f;


        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.LTGRAY);
        for (String s: shapes) {
            //Circle
            if (s.charAt(1) == 'C') {
                //X offset from middle point
                s = s.substring(3);
                int index = s.indexOf(';');
                float x = Float.parseFloat(s.substring(0, index));
                //Y offset from middle point
                s = s.substring(index + 1);
                index = s.indexOf(';');
                float y = Float.parseFloat(s.substring(0, index));
                //the circle's radius
                s = s.substring(index + 1);
                index = s.length()-1;
                float radius = Float.parseFloat(s.substring(0, index)) / 2;

                //Draw the circle shape
                canvas.drawCircle(baseX + x * scale,
                        baseY + y * scale,
                        radius * scale,
                        paint);

                //Rectangle
            } else if (s.charAt(1) == 'R') {
                //X offset from middle point
                s = s.substring(3);
                int index = s.indexOf(';');
                float x = Float.parseFloat(s.substring(0, index));
                //Y offset from middle point
                s = s.substring(index + 1);
                index = s.indexOf(';');
                float y = Float.parseFloat(s.substring(0, index));
                //width
                s = s.substring(index + 1);
                index = s.indexOf(';');
                float width = Float.parseFloat(s.substring(0, index));
                //height
                s = s.substring(index + 1);
                index = s.length()-1;
                float height = Float.parseFloat(s.substring(0, index));

                //Draw the rectangle
                canvas.drawRect(baseX + x * scale - width / 2 * scale,
                            baseY + y * scale - height / 2 * scale,
                            baseX + x * scale + width / 2 * scale,
                            baseY + y * scale + height / 2 * scale,
                            paint);
            }
        }
    }


    /**
     * Clear the shapes list, use it every time a new object is loaded
     */
    public void clearShapes() {
        shapes = new ArrayList<>();
    }

    /**
     * Add a shape to the list
     * @param s     the string describing a basic shape
     */
    public void addShape(String s) {
        shapes.add(s);
    }

    public void setPosition(float x, float y, float width, float height) {
        electrodeX = x;
        electrodeY = y;
        electrodeWidth = width;
        electrodeHeight = height;
    }

}
