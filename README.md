# OLIR: Off-Line Interactor Recognition

**O**ff-**L**ine **I**nteractor **R**ecognition is used to read-out whether an off-line sensor was triggered or not.

Such sensors are passive 3D-printed objects that detect one-time interactions, such as accelerating or flipping, but neither require active electronics nor power at the time of the interaction. They memorize a pre-defined interaction via an embedded structure filled with a conductive medium (e.g., a liquid). Whether a sensor was exposed to the interaction can be read-out via a capacitive touchscreen and OLIR. To create an off-line sensor, please see OLIP.

More information on off-line sensors can be found [here](https://www.tk.informatik.tu-darmstadt.de/de/research/tangible-interaction/off-line-sensing/).

![OLIR Screenshot](misc/olir.png)


-----



## Installing OLIR
See [Releases](../../releases)


-----


## Compiling and Building OLIR

#### Compiling
Plug in your Android device and run `./gradlew installDebug`

#### Building a Release APK
Run `./gradlew assemble`


-----


## Contributing
In case of any questions, please contact us via [mail](mailto:schmitz@tk.tu-darmstadt.de) or create an [issue](../../issues/).